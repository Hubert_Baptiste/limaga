import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProduitDerive {
	
	
	/**
	 * La taille du produit derive
	 */
	private String Taille;
	
	/**
	 * L'identifiant de ce produit derive
	 */
	private int Identifiant;
	
	
	/**
	 * La description de ce produit derive
	 */
	private String Description;
	
	
	public ProduitDerive(String t, int Id, String descr) throws ClassNotFoundException, SQLException {
		this.Taille = t;
		this.Identifiant = Id;
		this.Description = descr;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `ProduitDerive`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}
	
	public String toString() {
		return "Ce produit d�riv� de taille " + this.Taille + ", identifi� avec le code " + this.Identifiant +" est de type " + this.Description;
	}
	

}
