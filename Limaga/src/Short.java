import java.sql.SQLException;

public class Short extends ProduitDerive {
	
	/**
	 * La couleur du short
	 */
	private String Couleur;
	
	
	/**
	 * La quantit� de short
	 */
	private int Quantite;

	public Short(String t, int Id, String descr, String c, int q) throws ClassNotFoundException, SQLException {
		super(t, Id, descr);
		this.Couleur = c;
		this.Quantite = q;
	}

}
