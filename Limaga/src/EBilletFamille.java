import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EBilletFamille extends EBillet{

	public EBilletFamille(int n, java.sql.Date sysDate, String a, String descr, int pU, double tva, double pTTC)
			throws ClassNotFoundException, SQLException {
		super(n, sysDate, a, descr, pU, tva, pTTC);
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `EBilletFamille`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}

}
