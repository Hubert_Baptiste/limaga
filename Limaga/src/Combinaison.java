import java.sql.SQLException;

public class Combinaison extends Materiel{
	
	/**
	 * La taille de la combinaison
	 */
	private String Taille;
	
	/**
	 * Le sexe
	 */
	private String Sexe;

	public Combinaison(String t, int id, String desc, int p,String taille, String s) throws ClassNotFoundException, SQLException {
		super(t, id, desc, p);
		this.Taille = taille;
		this.Sexe = s;
		
	}

}
