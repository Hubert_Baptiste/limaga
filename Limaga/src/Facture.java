import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Facture {
	
	
	/**
	 * Le numero de la facture
	 */
	private int Numero;
	
	/**
	 * La date de la facture
	 */
	private Date date;
	

	/**
	 * Le total hors taxe de la facture
	 */
	private int TotalHT;
	
	/**
	 * Le total avec la tva de la facture
	 */
	private int TotalTVA;
	
	/**
	 * Le total avec toutes les taxes comprises
	 */
	private int TotalTTC;
	
	
	
	
	
	public Facture(int n, Date sysDate, int totalHT, int totalTva, int totalttc) throws ClassNotFoundException, SQLException {
		this.Numero = n;
		this.date = sysDate;
		this.TotalHT = totalHT;
		this.TotalTVA = totalTva;
		this.TotalTTC = totalttc;
		
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `Facture`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}
	
	
	
	
	

}
