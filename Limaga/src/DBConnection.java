import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

	/**
	 * 
	 * @author Gand Axel, Lemaitre Leo, Hubert Baptiste
	 *
	 */
	public class DBConnection {
		
		/**
		 * L'instance DBConnection
		 */
		private static DBConnection instance;
		
		/**
		 * La connection a la base de donnees
		 */
		private Connection connection;
		
		
		// variables a modifier en fonction de la base
		private static String userName = "root";
		//private static String password = "root";
		private static String serverName = "localhost";
		private static String portNumber = "3306";
		private static String dbName = "Limaga";

		// creation de la connection
			
		/**
		 * Le constructeur de la classe
		 * @throws SQLException
		 * @throws ClassNotFoundException 
		 */
		private DBConnection() throws SQLException, ClassNotFoundException{
			Properties connectionProps = new Properties();
			connectionProps.put("user", userName);
			String urlDB = "jdbc:mysql://" + serverName + ":";
			urlDB += portNumber + "/" + dbName;
			Class.forName("com.mysql.jdbc.Driver");
			try {
				connection = DriverManager.getConnection(urlDB, connectionProps);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * Methode mettant en oeuvre le patron singleton
		 * @return l'instance de DBConnection
		 * @throws SQLException
		 * @throws ClassNotFoundException 
		 */
		public synchronized static DBConnection getInstance() throws SQLException, ClassNotFoundException{
			if(instance == null){
				instance = new DBConnection();
			} 
			return instance;
		}
		
		
		/**
		 * Methode pour recuperer la connection
		 * @return la connection
		 * @throws SQLException
		 * @throws ClassNotFoundException 
		 */
		public Connection getConnection(){
			return connection;
		}
		
		/**
		 * Methode pour recuperer le nom de la database
		 * @return le nom de la db
		 * @throws SQLException 
		 * @throws ClassNotFoundException 
		 */
		public static String getDbName() throws ClassNotFoundException, SQLException{
			return dbName;
		}
		
		/**
		 * Methode pour donner un nom de database 
		 * @param dbname
		 * 			Le nom a donner
		 * @throws SQLException 
		 * @throws ClassNotFoundException 
		 */
		public static void setNomDB(String nomDB) throws ClassNotFoundException, SQLException{
			DBConnection.dbName = nomDB;	
			instance = new DBConnection();
		}

}

