import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MembreDeFamille {
	
	
	/**
	 * Le nom du client
	 */
	private String Nom;
	
	/**
	 * Le prenom du client
	 */
	private String Prenom;
	
	/**
	 * L'age du client
	 */
	private int Age;
	
	/**
	 * Le niveau de natation
	 */
	private String NiveauDeNatation;
	
	
	public MembreDeFamille(String n, String p, int a, String ndn) throws ClassNotFoundException, SQLException{
		this.Nom = n;
		this.Prenom = p;
		this.Age = a;
		this.NiveauDeNatation = ndn;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `MembreDeFamille`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}
	

	
	
	
	

}
