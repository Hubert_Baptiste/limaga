import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AbonnementFamille extends Abonnement {

	public AbonnementFamille(int numAbo, int nbEntree, String description) throws ClassNotFoundException, SQLException {
		super(numAbo, nbEntree, description);

		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `AbonnemenFamille`" + " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL," + "  `ID_REA` int(11) DEFAULT NULL, " + " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}

}
