import java.sql.SQLException;

public class Lunette extends ProduitDerive{
	
	/**
	 * Le nombre de lunette
	 */
	private int Quantite;

	public Lunette(String t, int Id, String descr, int q) throws ClassNotFoundException, SQLException {
		super(t, Id, descr);
		this.Quantite = q;
	}

}
