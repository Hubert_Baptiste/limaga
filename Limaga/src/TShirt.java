import java.sql.SQLException;

public class TShirt extends ProduitDerive{
	
	/**
	 * La couleur du Tshirt
	 */
	private String Couleur;
	
	
	/**
	 * La quantit� de Tshirt
	 */
	private int Quantite;

	public TShirt(String t, int Id, String descr,String c, int q) throws ClassNotFoundException, SQLException {
		super(t, Id, descr);
		this.Couleur = c;
		this.Quantite = q;
	}

}
