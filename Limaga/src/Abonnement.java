import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Abonnement {
	
	
	/**
	 * Le numero de l'abonnement
	 */
	private int NumeroAbo;
	
	
	/**
	 * Le nombre d'entree sur l'abonnement
	 */
	private int nbEntree;
	
	
	/**
	 * La description de l'abonnement
	 */
	private String Description;
	
	
	
	public Abonnement(int numAbo, int nbEntree, String description) throws ClassNotFoundException, SQLException {
		this.NumeroAbo = numAbo;
		this.nbEntree = nbEntree;
		this.Description= description;
		
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `Abonnement`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}
	
	//private CodeBarre Codebarre;

}
