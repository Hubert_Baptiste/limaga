import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class EBillet {

	/**
	 * Le numero du billet
	 */
	private int Numero;

	/**
	 * Le date du billet
	 */
	private String Date;

	/**
	 * La valeur associant l'acces grace au billet
	 */
	private String Acces;

	// private CodeBarre CodeBarre;

	/**
	 * La description du billet
	 */
	private String description;

	/**
	 * Le prix unitaire du billet
	 */
	private int PrixUnitaire;

	/**
	 * la TVA
	 */
	private double TVA;

	/**
	 * Le prix toute taxe comprise
	 */
	private double PrixTTC;

	public EBillet(int n, String Date, String a, String descr, int pU, double tva, double pTTC) throws ClassNotFoundException, SQLException {
		this.Numero = n;
		this.Date = Date;
		this.Acces = a;
		this.description = descr;
		this.PrixUnitaire = pU;
		this.TVA = tva;
		this.PrixTTC = pTTC;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `EBillet`"
				+ " (  'NuméroBillet' int(11) NOT NULL AUTO_INCREMENT,"
			//	+ "  'Date' TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' , "
				+ "  'Date' DATE NOT NULL , "
				+ "'Acces' varchar(40) NOT NULL,"
				+ "'Description' varchar(40) NOT NULL,"
				+ "'prixUnitaire'  Double(2,2) NOT NULL,"
				+ "'tva'  Double(2,2) NOT NULL,"
				+ "'PrixTTC'  DECIMAL(2,2) NOT NULL,"
				+ " PRIMARY KEY (`NuméroBillet`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}

}
