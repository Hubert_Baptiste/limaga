import java.sql.SQLException;

public class Palme extends Materiel {
	
	/**
	 * la pointure de la palme
	 */
	private int pointure;
	

	public Palme(String t, int id, String desc, int p, int pointure) throws ClassNotFoundException, SQLException {
		super(t, id, desc, p);
		this.pointure = pointure;
	}

}
