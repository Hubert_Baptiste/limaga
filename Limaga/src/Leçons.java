import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Le�ons {
	
	/**
	 * Le numero de billet de la le�on
	 */
	private int NumBillet;
	
	
	/**
	 * La description de la le�on
	 */
	private String Description;
	
	//private CodeBarre CodeBarre;
	
	
	public Le�ons(int nBillet, String description) throws ClassNotFoundException, SQLException{
		this.NumBillet=nBillet;
		this.Description = description;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `Le�ons`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}

}
