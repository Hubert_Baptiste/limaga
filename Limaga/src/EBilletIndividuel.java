import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EBilletIndividuel extends EBillet{
	
	private int Numero;
	
	private int NbEntreeMax;
	
	private String Date;
	
	private String Acces;

	public EBilletIndividuel(int n, String Date, String a, String descr, int pU, double tva, double pTTC, int nEMax)
			throws ClassNotFoundException, SQLException {
		
		super(n, Date, a, descr, pU, tva, pTTC);
		this.NbEntreeMax = nEMax;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `EBilletIndividuel`"
				+ " (  'NuméroBillet' int(11) NOT NULL AUTO_INCREMENT,"
					+ "'NombreEntreesMax'  INT(2) NOT NULL,"
					+ " PRIMARY KEY (`NuméroBillet`),"
					+ " constraint fk_id_real foreign key (NuméroBillet) references EBillet (EBilletIndividuel)) , "
					+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}

}
