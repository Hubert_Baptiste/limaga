import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import activeRecord.DBConnection;
import activeRecord.Film;

/**
 * 
 * @author Gand Axel, Lemaitre Leo, Hubert Baptiste
 *
 */

public class Client{
	
	
	/**
	 * Le nom du client
	 */
	private String Nom;
	
	/**
	 * Le prenom du client
	 */
	private String Prenom;
	
	
	/**
	 * L'identifiant du client
	 */
	private String ID;
	
	/**
	 * Le mot de passe du client
	 */
	private String mdp;
	
	/**
	 * Le statut de connexion du client
	 */
	private boolean connecte;
	
	
	/**
	 * La liste de facture du client
	 */
	private List facture;
	
	
	
	/**
	 * Le client a un panier
	 */
	private Panier Panier;
	
	
	public Client(String n, String p, String i, String m) throws ClassNotFoundException, SQLException {
		this.Nom = n;
		this.Prenom = p;
		this.ID = i;
		this.mdp = m;
		this.connecte = true;
		this.facture = new ArrayList<Facture>();
		
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s =  "CREATE TABLE if not exists Client ( " + "ID INTEGER  AUTO_INCREMENT, "
				+ "NOM varchar(40) NOT NULL, " + "PRENOM varchar(40) NOT NULL, " + "MDP varchar(40) not null," + " PRIMARY KEY (ID))";
	
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
		
		String SQLPrep = "INSERT INTO Client (nom, prenom, MDP) VALUES (?,?,?);";
		PreparedStatement prep1;
		// l'option RETURN_GENERATED_KEYS permet de recuperer l'id (car
		// auto-increment)
		prep1= connect.prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep1.setString(1, n);
		prep1.setString(2, p);
		prep1.setString(3, m);
		prep1.executeUpdate();
		System.out.println("2 ajout de "+this);
	}
	
	
	/**
	 * Methode pour supprimer la table Film
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public static void deleteTable() throws SQLException, ClassNotFoundException{
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "DROP TABLE Client" ;
		java.sql.Statement prep = connect.createStatement();
		prep.executeUpdate(s);
	}
	
	public void reserverBillet(){
		
		
	}
	
	public List<EBilletIndividuel> choisirDateAcces(String date,String acces){
		List<EBilletIndividuel> result = new ArrayList<EBilletIndividuel>();
		String req="Select * from ebillet where date="+date+" and acces="+acces+";";
		Connection connect = DBConnection.getInstance().getConnection();
		PreparedStatement prep1 = connect.prepareStatement(req);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		EBilletIndividuel eBillet;
		while (rs.next()) {
			int numeroBillet = rs.getInt("NumeroBillet");
			int nbEntrees=rs.getInt("nbEntreesMax");
			int idReal = rs.getInt("id_real");
			eBillet = new EBilletIndividuel(numeroBillet, , a, descr, pU, tva, pTTC, nEMax);
			System.out.println(eBillet);
		}
		return result;
	}
		
	
		

	
	

	
}

