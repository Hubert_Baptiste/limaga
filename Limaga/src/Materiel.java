import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Materiel {
	
	/**
	 * le type de materiel
	 */
	private String Type;
	
	
	/**
	 * l'identifiant de ce materiel
	 */
	private int Identifiant;
	
	
	/**
	 * La descrtiption de ce materiel
	 */
	private String Description;
	
	/**
	 * Le prix de ce materiel
	 */
	private int Prix;
	
	
	public Materiel(String t, int id, String desc, int p) throws ClassNotFoundException, SQLException {
		this.Type = t;
		this.Identifiant = id;
		this.Description = desc;
		this.Prix = p;
		
		Connection connect = DBConnection.getInstance().getConnection();
		String s = "CREATE TABLE IF NOT EXISTS `Materiel`"
				+ " (  `ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "  `TITRE` varchar(40) NOT NULL,"
				+ "  `ID_REA` int(11) DEFAULT NULL, "
				+ " PRIMARY KEY (`ID`),"
				+ " ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
		PreparedStatement prep = connect.prepareStatement(s);
		prep.executeUpdate();
	}
	
	public String toString() {
		return "Ce materiel de type " + this.Type + ", coutant "+this.Prix +" identifiť avec le code " + this.Identifiant +" est de type " + this.Description;
	}
	

}
